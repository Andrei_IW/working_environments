﻿<html>
	<head>
		<title>Стартовая страница</title>
	</head>
	<body style="background: url(images/bg.jpg) top center no-repeat; font-family: Arial; color:#fff;">
		<div style="width:900px; margin:0 auto;">
			<h1 style="text-align:center; font-size:46px; margin-top:160px;">Стартовая страница!</h1>
			<p style="font-size:20px; margin-top:50px; border-bottom: 1px solid #fff; padding-bottom:40px;">Эта страница является стартовой для web окружения собранного на Docker конейнерах. Ниже ссылки на дополнительное ПО, развёрнутое в этом окружении:</p>
			<div style="width:33%; float:left; color: #555; text-align:center; font-size:24px;">
				<a href="http://<?=$_SERVER['SERVER_NAME']?>:8000">PHP MyAdmin</a>
			</div>
			<div style="width:33%; float:left; color: #555; text-align:center; font-size:24px;">
				<a href="http://<?=$_SERVER['SERVER_NAME']?>:8001">Adminer</a>
			</div>
			<div style="width:33%; float:left; color: #555; text-align:center; font-size:24px;">
				<a href="phpinfo.php">PHP Info</a>
			</div>
		</div>
	</body>
</html>
